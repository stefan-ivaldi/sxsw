import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './card.module.scss';

const cx = classNames.bind(styles);

const Card = ({
  className,
}) => {
  const classes = cx(
    {
      card: true,     
    },
    className
  );

  return (
    <div className={classes}>
  
    </div>
  );
};

Card.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

Card.defaultProps = {
  children: null,
  className: '',
};

// Needed for Storybook
Card.displayName = 'Card';

export default Card;
